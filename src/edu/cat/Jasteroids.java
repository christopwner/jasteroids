/*
 * Copyright (c) 2016, Christopher Allen Towner
 * All rights reserved.
 */
package edu.cat;

import java.util.Optional;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Asteroids demo in Java.
 */
public class Jasteroids extends Application {

    private static final double WIDTH = 600;
    private static final double HEIGHT = 400;

    private static final double ROTATE_SPEED = 5;
    private static final double THRUST_SPEED = .2;
    private static final double THRUST_MAX = 5;
    private static final double THRUST_MIN = THRUST_MAX / 4;

    private static final double TIMELINE_SPEED = 0.016;

    private double vx = 0;  //x velocity
    private double vy = 0;  //y velocity

    private double r = 0;  //rotation

    private boolean accelerating = false;
    private boolean decelerating = false;

    private boolean fullscreen = false;

    private final Double[] shipPoints = new Double[]{
        0.0, 0.0,
        7.5, 20.0,
        -7.5, 20.0
    };

    private final Double[] thrustPoints = new Double[]{
        7.5, 20.0,
        5.0, 20.0,
        2.5, 22.5,
        0.0, 20.0,
        -2.5, 22.5,
        -5.0, 20.0,
        -7.5, 20.0
    };

    @Override
    public void start(Stage primaryStage) {
        StackPane root = new StackPane();

        //setup ship and add to pane
        Polygon ship = new Polygon();
        ship.getPoints().addAll(shipPoints);
        ship.setStroke(Color.BLACK);
        ship.setFill(Color.WHITE);
        root.getChildren().add(ship);

        //setup scene with key events
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        scene.setOnKeyPressed((KeyEvent key) -> {
            switch (key.getCode()) {
                case UP:
                    accelerating = true;
                    //add thrust effect
                    ship.getPoints().addAll(thrustPoints);
                    break;
                case DOWN:
                    decelerating = true;
                    break;
                case LEFT:
                    r = -ROTATE_SPEED;
                    break;
                case RIGHT:
                    r = ROTATE_SPEED;
                    break;
                case F11:
                    primaryStage.setFullScreen(fullscreen = !fullscreen);
                    break;
            }
        });
        scene.setOnKeyReleased((KeyEvent key) -> {
            switch (key.getCode()) {
                case UP:
                    accelerating = false;
                    //remove thrust effect
                    ship.getPoints().clear();
                    ship.getPoints().addAll(shipPoints);
                    break;
                case DOWN:
                    decelerating = false;
                case LEFT:
                case RIGHT:
                    r = 0;
                    break;
            }
        });

        //setup game loop and start
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(TIMELINE_SPEED),
                ae -> {
                    //check velocity
                    if (accelerating) {
                        vx += THRUST_SPEED * Math.cos(Math.toRadians(ship.getRotate() - 90));
                        vy += THRUST_SPEED * Math.sin(Math.toRadians(ship.getRotate() - 90));
                        if (vx < -THRUST_MAX) {
                            vx = -THRUST_MAX;
                        }
                        if (vx > THRUST_MAX) {
                            vx = THRUST_MAX;
                        }
                        if (vy < -THRUST_MAX) {
                            vy = -THRUST_MAX;
                        }
                        if (vy > THRUST_MAX) {
                            vy = THRUST_MAX;
                        }
                    } else if (decelerating) {
                        vx -= (THRUST_SPEED / 4) * Math.cos(Math.toRadians(ship.getRotate() - 90));
                        vy -= (THRUST_SPEED / 4) * Math.sin(Math.toRadians(ship.getRotate() - 90));
                        if (vx < -THRUST_MIN) {
                            vx = -THRUST_MIN;
                        }
                        if (vx > THRUST_MIN) {
                            vx = THRUST_MIN;
                        }
                        if (vy < -THRUST_MIN) {
                            vy = -THRUST_MIN;
                        }
                        if (vy > THRUST_MIN) {
                            vy = THRUST_MIN;
                        }
                    }

                    //update ship position with velocity
                    ship.setTranslateX(ship.getTranslateX() + vx);
                    ship.setTranslateY(ship.getTranslateY() + vy);

                    //check position
                    final double X_MIN = -(scene.getWidth() / 2);
                    final double X_MAX = (scene.getWidth() / 2);
                    final double Y_MIN = -(scene.getHeight() / 2);
                    final double Y_MAX = (scene.getHeight() / 2);
                    if (ship.getTranslateX() < X_MIN) {
                        ship.setTranslateX(X_MAX);
                    }
                    if (ship.getTranslateY() < Y_MIN) {
                        ship.setTranslateY(Y_MAX);
                    }
                    if (ship.getTranslateX() > X_MAX) {
                        ship.setTranslateX(X_MIN);
                    }
                    if (ship.getTranslateY() > Y_MAX) {
                        ship.setTranslateY(Y_MIN);
                    }

                    //check rotation
                    ship.setRotate(ship.getRotate() + r);
                    if (ship.getRotate() == -10) {
                        ship.setRotate(350);
                    }
                    if (ship.getRotate() % 360 == 0) {
                        ship.setRotate(0);
                    }
                }
        ));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        //set stage and show
        primaryStage.setTitle("Jasteroids");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
